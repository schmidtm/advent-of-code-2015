var Advent = {
	Day1 : function () {
		var vals = document.getElementById("inp").value,
			res = document.getElementById("res"),
			neg = vals.replace(/[^(]/gi, "").length,
			pos = vals.replace(/[^)]/gi, "").length,
			tmp = neg - pos;
		/* if ( tmp >= 0)
			tmp++; */
		res.value = tmp;
	},
	Day1Part2: function () {
		var vals = document.getElementById("inp").value,
			res = document.getElementById("res"),
			currentFloor = 0,
			inc = 0,
			i = 0;
		for (i = 0; i < vals.length; i++) {
			inc = 0;
			switch (vals[i]) {
			case "(":
				inc = 1;
				break;
			case ")":
				inc = -1;
				break;
			}
			currentFloor += inc;
			if (currentFloor < 0) {
				res.value = i + 1;
				break;
			}
		}
	},
	Day2Part1: function()
	{
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var val = 0;
		var totVal = 0;
		var tmpRes = [];
		for ( var i = 0; i < vals.length; i++ )
		{
			val = this.surfaceAreaPlusSlack(vals[i]);
			tmpRes.push(vals[i] + "=" + val);
			totVal += val;
		}
		tmpRes.push("Total: " + totVal);
		res.value = tmpRes.join("\n");
	},
	surfaceAreaPlusSlack: function(dim)
	{

		var arr = dim.split("x");            
		//arr.sort();
		/* l x w x h  smallest  of lxh or wxh */
		var lw = arr[0] * arr[1];
		var lh = arr[0] * arr[2];
		var wh = arr[1] * arr[2];
		var extra = Math.min(lw, Math.min(lh,wh));
		return 2 * ( lw + lh + wh ) + extra;

	},
	Day2Part2: function()
	{
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var val = 0;
		var totVal = 0;
		var tmpRes = [];
		for ( var i = 0; i < vals.length; i++ )
		{
			val = this.smallestDiameter(vals[i]);
			tmpRes.push(vals[i] + "=" + val);
			totVal += val;
		}
		tmpRes.push("Total: " + totVal);
		res.value = tmpRes.join("\n"); 
	},
	smallestDiameter: function(dim)
	{
		  var arr = dim.split("x");            
		var tmp = 0;
		var bow = arr[0] * arr[1] * arr[2];
		var lw = 1*arr[0] + 1*arr[1];
		var lh = 1*arr[0] + 1*arr[2];
		var wh = 1*arr[1] + 1*arr[2];

		tmp = Math.min(lw, Math.min(lh, wh))*2;

		return tmp + bow;
	},
	
	LoadReadMe: function()
	{
		 var xhr = new XMLHttpRequest();
	xhr.open("get", "README.txt", true);
	if ( ! xhr["onload"] )
	{

		xhr.onreadystatechange = this.onLoadReadMe_readyStateChange.bind(this, xhr);
	} else {			
		xhr.onload = this.onLoadReadMe.bind(this, xhr);
	}
	xhr.send([]);
	},
	onLoadReadMe_readyStateChange: function(resp) {
		if ( resp.readyState == 4)
	{
		if ( resp.status == 200 || resp.status == 304)
		{
			this.onLoadReadMe(resp);
		}
	}

	},
	onLoadReadMe: function(xhr)
	{
	   document.querySelector("pre").innerHTML = xhr.responseText;
	},
	LoadInput: function()
	{
	  var xhr = new XMLHttpRequest();
	xhr.open("get", "input.txt", true);
	if ( ! xhr["onload"] )
	{
		xhr.onreadystatechange = this.onLoadInput_readyStateChange.bind(this, xhr);
	} else {			
		xhr.onload = this.onLoadInput.bind(this, xhr);
	}
	xhr.send([]);
	},
		onLoadInput_readyStateChange: function(resp)
{
	if ( resp.readyState == 4)
	{
		if ( resp.status == 200 || resp.status == 304)
		{
			this.onLoadInput(resp);
		}
	}

},
	onLoadInput: function(xhr)
	{
	   document.getElementById("inp").value= xhr.responseText;
	},
	Day3Part1: function()
	{
		/* deal with coordinates system as a keys to struct */
		var vals = document.getElementById("inp").value
		var res = document.getElementById("res");

		var travels = {};
		travels["0,0"] = true;
		var current=[0,0];
		for ( var i = 0; i < vals.length; i++ )
		{
			switch (vals[i])
			{
				case "^":
					current[1]++;
					break;
				case "v":
				case "V" :
					current[1]--;
					break;
				case ">":
					current[0]++;
					break;
				case "<":
					current[0]--;
					break;
			}
			travels[current[0] +"," + current[1]] = true;
		}

	  res.value = this.objSize(travels);  

	},
	Day3Part2: function()
	{
		 var vals = document.getElementById("inp").value
		var res = document.getElementById("res");

		var travels = {};
		travels["0,0"] = true;
		var current=[[0,0], [0,0]];
		for ( var i = 0; i < vals.length; i++ )
		{
			var roboVSanta = i % 2;
			switch (vals[i])
			{
				case "^":
					current[roboVSanta][1]++;
					break;
				case "v":
				case "V" :
					current[roboVSanta][1]--;
					break;
				case ">":
					current[roboVSanta][0]++;
					break;
				case "<":
					current[roboVSanta][0]--;
					break;
			}
			travels[current[roboVSanta][0] +"," + current[roboVSanta][1]] = true;
		}

	  res.value = this.objSize(travels);  
	},
	objSize: function(obj)
	{
		var size = 0, key;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;

	},
	Day4Part1: function()
	{
		var vals = document.getElementById("inp").value
		var res = document.getElementById("res");
		for ( var i = 1; ; i++)
		{
			var test = md5(vals + i.toString());   
			if ( test.substring(0,5) == "00000" )
			{
				res.value = i;
				break;
			}

		}

	},
	Day4Part2: function()
	{
		var vals = document.getElementById("inp").value
		var res = document.getElementById("res");
		for ( var i = 1; ; i++)
		{
			var test = md5(vals + i.toString());   
			if ( test.substring(0,6) == "000000" )
			{
				res.value = i;
				break;
			}

		}

	},
	Day5Part1: function()
	{  
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var out = [];
		var cntNice = 0;
		for ( var i = 0; i < vals.length; i++ )
		{
			if ( this.niceString(vals[i]) )
			{
				out.push("Nice");
				cntNice++;
			}
			else
				out.push("Naughty");
		}
		out.push("Total Nice:" + cntNice);
		res.value = out.join("\n");

	},
	niceString: function(strIn)
	{
		if ( /(ab)|(cd)|(pq)|(xy)/gi.test(strIn) )
			return false;
		if (! /(.)\1/gi.test(strIn) )
			return false;
		if ( ! /[aeiou].*[aeiou].*[aeiou]/gi.test(strIn) )
			return false;
		return true;
	},
	Day5Part2: function()
	{
			   var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var out = [];
		var cntNice = 0;
		for ( var i = 0; i < vals.length; i++ )
		{
			if ( this.niceStringPart2(vals[i]) )
			{
				out.push("Nice");
				cntNice++;
			}
			else
				out.push("Naughty");
		}
		out.push("Total Nice:" + cntNice);
		res.value = out.join("\n");
	},
  niceStringPart2: function(strIn)
	{
		if (! /(..).*\1/gi.test(strIn) )
			return false;
		if (! /(.).\1/gi.test(strIn) )
			return false;

		return true;
	},
	Day6Part1: function()
	{
		var initSquare = function(width,height) { 
			var ret = [];
			for ( var i = 0; i < width; i++ )
			{
				ret[i] = [];
				for ( var j=0; j< height; j++ )
				{
					ret[i][j] = false;
				}
			}
			return ret;
		}

		var doOverRange = function(corn1, corn2, act, arr) 
		{
			 corn1 = corn1.split(",");
			 corn2 = corn2.split(",");
			var fromX = Math.min(corn1[0], corn2[0]);
			var toX = Math.max(corn1[0], corn2[0]);
			var fromY = Math.min(corn1[1], corn2[1]);
			var toY = Math.max(corn1[1], corn2[1]);
			for ( var i= fromX; i <= toX; i++)
			{
				for ( var j = fromY; j<= toY; j++)
				{
					switch(act)
					{
						case 0 :
							arr[i][j] = false;
							break;
						case 1 :
							arr[i][j] = true;
							break;
						case 2 :
							arr[i][j] = ! arr[i][j];
							break;
					}
				}
			}
		}

		var countTrue = function(arr)
		{
			var tot = 0;
			for ( var i = 0; i < arr.length; i++ )
			{
				for ( var j = 0; j< arr[i].length; j++ )
				{
					if ( arr[i][j] )
							tot++;
				}
			}
			return tot;
		}
		var newArr = initSquare(1000,1000)

		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		for ( var i = 0; i < vals.length; i++ )
		{
			var tmpInp = vals[i].split(" ");   
			if (tmpInp[0] == "toggle" )
			{
			   doOverRange(tmpInp[1], tmpInp[3], 2, newArr); 
			} else if (tmpInp[1] == "off" ) {
				doOverRange(tmpInp[2], tmpInp[4], 0, newArr);
			} else if (tmpInp[1] == "on" ) {
				doOverRange(tmpInp[2], tmpInp[4], 1, newArr);
			}
		}
		res.value = countTrue(newArr);
	},
	   Day6Part2: function()
	{
		var initSquare = function(width,height) { 
			var ret = [];
			for ( var i = 0; i < width; i++ )
			{
				ret[i] = [];
				for ( var j=0; j< height; j++ )
				{
					ret[i][j] = 0;
				}
			}
			return ret;
		}

		var doOverRange = function(corn1, corn2, act, arr) 
		{
			 corn1 = corn1.split(",");
			 corn2 = corn2.split(",");
			var fromX = Math.min(corn1[0], corn2[0]);
			var toX = Math.max(corn1[0], corn2[0]);
			var fromY = Math.min(corn1[1], corn2[1]);
			var toY = Math.max(corn1[1], corn2[1]);
			for ( var i= fromX; i <= toX; i++)
			{
				for ( var j = fromY; j<= toY; j++)
				{
					switch(act)
					{
						case 0 :
							arr[i][j]--;
							if ( arr[i][j] < 0 )
								arr[i][j] = 0;
							break;
						case 1 :
							arr[i][j]++;
							break;
						case 2 :
							arr[i][j]+= 2;
							break;
					}
				}
			}
		}

		var countTrue = function(arr)
		{
			var tot = 0;
			for ( var i = 0; i < arr.length; i++ )
			{
				for ( var j = 0; j< arr[i].length; j++ )
				{
					tot += arr[i][j]                            
				}
			}
			return tot;
		}
		var newArr = initSquare(1000,1000)

		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		for ( var i = 0; i < vals.length; i++ )
		{
			var tmpInp = vals[i].split(" ");   
			if (tmpInp[0] == "toggle" )
			{
			   doOverRange(tmpInp[1], tmpInp[3], 2, newArr); 
			} else if (tmpInp[1] == "off" ) {
				doOverRange(tmpInp[2], tmpInp[4], 0, newArr);
			} else if (tmpInp[1] == "on" ) {
				doOverRange(tmpInp[2], tmpInp[4], 1, newArr);
			}
		}
		res.value = countTrue(newArr);
	},
	Day7Part1: function()
	{
		 var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var convertToBinary = function(numIn)
		{
		 try {   var zeros = "0000000000000000000000000000";
			 var bits = numIn.toString(2).substr(0,16);
			  if ( bits.length < 16 )
			  {
				   bits  = zeros.substr(0, 16 - bits.length) + bits;
			  }

			 } catch (e) {                     
				 throw e; 
			 }  
			return bits;
		}
		var perOp = function(str, res, op, operand1, operand2) {


			switch ( op.toLowerCase() ) {
				case "assign" : 
					str[res] = operand1 *1;
					break;
				case "and" :
					{
						var bits1 = "";
						var bits2 = "";
						if ( isInteger(operand1) )
						{
							bits1 = convertToBinary(operand1*1);
						} else {
							bits1 = convertToBinary(str[operand1]);
						}
						if ( isInteger(operand2) )
						{
							bits2 = convertToBinary(operand2*1);
						} else {
							bits2 = convertToBinary(str[operand2]);
						}
						var aft = [];

						for ( var i = 0; i < 16; i++ )
						{
						   aft[i] = parseInt(bits1[i]) & parseInt(bits2[i]); 

						}
						var aft2 = aft.join("");
						str[res] = parseInt(aft2, 2);
					}
					break;
				case "or" :
				   {
					  var bits1 = "";
						var bits2 = "";
						if ( isInteger(operand1) )
						{
							bits1 = convertToBinary(operand1*1);
						} else {
							bits1 = convertToBinary(str[operand1]);
						}
						if ( isInteger(operand2) )
						{
							bits2 = convertToBinary(operand2*1);
						} else {
							bits2 = convertToBinary(str[operand2]);
						}
						var aft = [];
					   for ( var i = 0; i < 16; i++ )
						{
						   aft[i] = parseInt(bits1[i]) | parseInt(bits2[i]); 
						}
						var aft2 = aft.join("");
						str[res] = parseInt(aft2, 2);
					}
					break;
				case "not" :
					{
					   var bits1 = [];
						if ( isInteger(operand1) )
						{
							bits1 = convertToBinary(operand1*1);
						} else {
							bits1 = convertToBinary(str[operand1]);
						}

						 var aft = [];
						for ( var i = 0; i < 16; i++ )
						{
							if ( bits1[i] == "0" )
								aft[i] = "1";
							else 
								aft[i] = "0";

						}
						var aft2 = aft.join("");
						str[res] = parseInt(aft2, 2);
					}
					break;
				case "lshift" :
					{
					   var bits1 = [];
						if ( isInteger(operand1) )
						{
							bits1 = convertToBinary(operand1*1);
						} else {
							bits1 = convertToBinary(str[operand1]);
						}

						bits1 = bits1.split("");
					   bits1.splice(0,Math.min(16,operand2))
					   bits1 = bits1.join("") + "0000000000000000000000000";
						bits1= bits1.substr(0,16);
					//str[res] = str[operand1] << operand2;
						str[res] = parseInt(bits1,2);
					}
					break;
				case "rshift" :
					  var bits1 = "";
						if ( isInteger(operand1) )
						{
							bits1 = convertToBinary(operand1*1);
						} else {
							bits1 = convertToBinary(str[operand1]);
						}

					   // bits1 = bits1.split("");
					   bits1 =  "0000000000000000000000000".substr(0,operand2) + bits1;

						bits1= bits1.substr(0,16);
					//str[res] = str[operand1] << operand2;
						str[res] = parseInt(bits1,2);
				   // str[res] = str[operand1] >>> operand2;
					break;
			}
		}
		var isInteger = function(str) {
			return /^[0-9]{1,}$/gi.test(str);   
		}
		var str = {};
		var lastLen = 10000000;
		while ( vals.length > 0 ) 
		{
		for (var i = 0; i < vals.length; i++ )
		{
			 var tmpHol = vals[i].split(" ");
			if (tmpHol.length == 3)
			{
				if ( isInteger(tmpHol[0]) )
				{
					perOp(str, tmpHol[2], "assign", tmpHol[0]);
					vals.splice(i,1);
					i--;
				} else if (str[tmpHol[0]] !== undefined ) {
					perOp(str, tmpHol[2], "assign", str[tmpHol[0]]);
					vals.splice(i,1);
					i--;

				}
			} else if ( tmpHol.length == 5) {
				if ( tmpHol[1] == "AND" || tmpHol[1] == "OR" )
				{
					if (  ( isInteger(tmpHol[0]) ||  str[tmpHol[0]] !== undefined  )  && (  isInteger(tmpHol[2]) || str[tmpHol[2]] !== undefined ))
					{
						perOp(str, tmpHol[4], tmpHol[1], tmpHol[0], tmpHol[2]);
						 vals.splice(i,1);
						i--;
					}
				} else {
					if ( isInteger(tmpHol[0]) ||  str[tmpHol[0]] !== undefined  )
					{
						perOp(str, tmpHol[4], tmpHol[1], tmpHol[0], tmpHol[2]);
						vals.splice(i,1);
						i--;
					}
				}
			} else if ( tmpHol.length == 4 ) {
				if (  isInteger(tmpHol[1]) ||  str[tmpHol[1]] !== undefined  )
				{
					perOp(str, tmpHol[3], "not", tmpHol[1]);     
					vals.splice(i,1);
					i--;
				}
			}
		}
			if ( vals.length == lastLen )
			{
				alert("Not Simplyifing");
				window.console.log(vals);
				break;
			} else {
				lastLen = vals.length;
			}
		}



		res.value = this.objToStr(str);
	},
	 objToStr: function(obj)
	{
		var str = [];
		var size = 0, key;
		for (key in obj) {
			if (obj.hasOwnProperty(key))
			{
				str.push(key + ":" + obj[key].toString());   
			}
		}
		return str.join("\n");

	},
	Day8Part1: function()
	{
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var totStr = 0;
		var totCode = 0;
	  var outStr = [];
		for ( var i = 0;i< vals.length; i++)
		{
			var stuff = vals[i].trim();
			totCode += stuff.length;
			var tmpStr = stuff.replace(/(\\")|(\\x[0-9a-f]{2})|(\\){2}/gi, " ").replace(/\"/gi, "");
			outStr.push(tmpStr);
			totStr += (stuff.length - tmpStr.length);
			outStr.push(stuff + "::" + stuff.length.toString() + "-" + ( stuff.length - tmpStr.length));
		}
		outStr.push(totCode.toString() + " - " + totStr.toString() + " = " + (totCode-totStr));
		res.value = outStr.join("\n");
	},
	Day8Part2: function() {
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
	   var totStr = 0;
		var totCode = 0;
	  var outStr = [];
		for ( var i = 0;i< vals.length; i++)
		{
			var stuff = vals[i].trim();
			totCode += stuff.length;
			var tmpStr = "\"" + stuff.replace(/(\\)/gi, "\\\\").replace(/(")/gi, "\\\"") + "\"";
			outStr.push(tmpStr);
			totStr += (tmpStr.length);
			//outStr.push(stuff + "::" + stuff.length.toString() + "-" + ( stuff.length - tmpStr.length));
		}
		outStr.push(totCode.toString() + " - " + totStr.toString() + " = " + (totCode-totStr));
		res.value = outStr.join("\n");
	},

	Day9Part1: function()
	{
		var parseRoute= function(lineIn)
		{
			var ret = {city1:"",city2:"",dist:0};
			var res = lineIn.split(" ");
			ret.city1 = res[0];
			ret.city2 = res[2];
			ret.dist = res[4];
			return ret;
		}
		var calcRoute = function(arrIn, elm, currentPath, currentDist)
		{
			var newArr = arrIn.slice();

			var curCit = newArr[elm];

			newArr.splice(elm,1);
			if (  newArr.length === 0  ) 
			{
				trips[currentPath] = currentDist;
				return;
			}
			for ( var i = 0; i < newArr.length; i++)
			{
				if ( dist[curCit + "_" + newArr[i]] )
				{
					calcRoute(newArr, i, currentPath + "_" + newArr[i], currentDist +  dist[curCit + "_" + newArr[i]]);							
				} else {
					return;	
				}
			}


		}

		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		var cities = {};
		var dist = {};
		var trips = {};
		for ( var i = 0; i < vals.length; i++)
		{
			var resu = parseRoute(vals[i]);
			cities[resu.city1] = true;
			cities[resu.city2] = true;
			dist[resu.city1 + "_" + resu.city2] = parseInt(resu.dist, 10);
			dist[resu.city2 + "_" + resu.city1] = parseInt(resu.dist, 10);

		}
		var citArr = [];
		for ( var i in cities )
		{
			citArr.push(i);
		}

		for ( var i = 0; i < citArr.length; i++ )
		{
			   calcRoute(citArr, i, citArr[i], 0)

		}
		var min = undefined;
		var minTrip = "";
		for ( var i in trips )
		{
			if ( min === undefined )
			{
				min=trips[i];
				minTrip = i;
			} else if ( min > trips[i] ) {
				minTrip = i;
				min= trips[i];

			}
		}

		var max = undefined;
		var maxTrip = "";
		for ( var i in trips )
		{
			if ( max === undefined )
			{
				max=trips[i];
				maxTrip = i;
			} else if ( max < trips[i] ) {
				maxTrip = i;
				max= trips[i];

			}
		}


		outStr.push("Min Trip:" +   minTrip + "::" + min );
		outStr.push("max trip:" + maxTrip + "::" + max);
		res.value = outStr.join("\n");
	},

	Day9Part2: function()
	{
		  var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		res.value = outStr.join("\n");
	},

	Day10Part1: function()
	{
		var expandStuff = function(inp) {
			var out = [];
			var matchy = /^(\d)\1*/;
			while (inp.length > 0 )
			{
				var res = matchy.exec(inp);   
				out.push(res[0].length);
				out.push(res[0].charAt(0));
				inp = inp.substr(res[0].length);
			}
			return out.join("");
		}

		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
	   var inpu = parseInt(vals[0]).toString();

		var numLoops = parseInt(vals[1]);
		for ( var i = 0; i < numLoops; i++)
		{
			var result = expandStuff(inpu);
			   outStr.push(result.length);
			inpu = result;
		}
		 res.value = outStr.join("\n");
	},
	Day11Part1: function()
	{
		var chars = "abcdefghjkmnpqrstuvwxyz".split("");
		var charsObj = {};
		for ( var i = 0; i< chars.length; i++)
			charsObj[chars[i]] = i;

		var testVal = function(valIn) {
			var arr = valIn.split("");
			var repeatTest = /(.)\1.?(.)\2/g;
			var badLetters = /^[^ilo]*$/g;

			if ( repeatTest.test(valIn) && badLetters.test(valIn) )
			{
				for ( var i = 0; i < arr.length-2;i++ )
				{
					if ( charsObj[arr[i]] + 2 == charsObj[arr[i+1]] + 1 && charsObj[arr[i+1]] + 1 == charsObj[arr[i+2]] )
						return true;
				}
			}
			return false;
		}

		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		var lastChar = chars[chars.length- 1];

		for ( var i = 0; i < vals.length; i++)
		{
			/* increment last character */
			outStr.push("starting::" + vals[i]);
			var arr = vals[i].split("");
			do {

				if ( arr[arr.length-1]  === lastChar )
				{
					var j = 1;
					while ( arr[arr.length-j] === lastChar )
					{

						arr[arr.length-j] = chars[0];
						j++;
					}
					arr[arr.length-j] = chars[charsObj[arr[arr.length-j]]+1];


				} else {
					arr[arr.length-1] = chars[charsObj[arr[arr.length-1]]+1];	
				} 

			} while(! testVal(arr.join("")) )
			outStr.push(arr.join(""));
		}

		res.value = outStr.join("\n");
	},
	Day12Part1: function()
	{
			var vals = document.getElementById("inp").value;
		var res = document.getElementById("res");
		var outStr = [];
		var tot = 0; 
		(vals.match(/([-]?\d*)/g).forEach(function(itm) { if ( ! isNaN(itm)  ) tot += itm*1 } ));
		outStr.push(tot);
		res.value = outStr.join("\n");
	},

			Day12Part2: function()
	{
		hasRed = function(obj) {
			if (typeof(obj) == "object")
			{
				for ( var i in obj ) 
				{
					if ( typeof(obj[i]) != "object" && obj[i] === "red" )
						return true;
				}
				return false;
			}
			return false;
		}
		sumObj = function(obj) {

			var subTot = 0;
			if ( Array.isArray(obj) )
			{
				obj.forEach(function(itm) {					
					if ( typeof(itm) == "object" )
					{
						subTot += sumObj(itm);
					} else if (! isNaN(itm) ) {
						subTot+= itm * 1;
					}
				});
			}
			else if (typeof(obj) == "object" && ! hasRed(obj))
			{

				for ( var i in obj ) 
				{					
					var val = obj[i];
					if ( typeof(val) == "object")
						subTot += sumObj(val)
					 else if (! isNaN(val) ) {
						subTot+= val * 1;
					}

				}

			} else {

			}
			return subTot;
		}
		var vals = document.getElementById("inp").value;
		var res = document.getElementById("res");
		var outStr = [];
		var tot = 0; 
		var dat = JSON.parse(vals);

		outStr.push(sumObj(dat));
		res.value = outStr.join("\n");
	},
	Day13Part1: function()
	{
		var calcRoute = function(arrIn, elm, currentPath, currentDist)
		{
			var newArr = arrIn.slice();

			var curCit = newArr[elm];

			newArr.splice(elm,1);
			if (  newArr.length === 0  ) 
			{

				var elm = currentPath.split("_");
				var tmpElm = [elm[0], elm[elm.length-1]];
				tmpElm.sort();
				trips[currentPath] =currentDist + cfg[tmpElm.join("_")];
				//trips[currentPath] = currentDist;
				return;
			}
			for ( var i = 0; i < newArr.length; i++)
			{
				var elm = [curCit, newArr[i]];
				elm.sort();
				if ( cfg[elm.join("_")] )
				{
					calcRoute(newArr, i, currentPath + "_" + newArr[i], currentDist +  cfg[elm.join("_")]);							
				} else {
					return;	
				}
			}


		}

		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		/* [0] is Person [2] gain lose [3] value [10] person followed by ending . */
		var ppl = {};
		var cfg = {};
		var trips = {};
		var dist = {};
		vals.forEach(function(itm) {

			var vals = itm.split(" ");
			ppl[vals[0]] = true;
			var chg = 1;
			if ( vals[2] == "lose" )
				chg = -1;
			var lookup = [ vals[0],  vals[10].slice(0,-1)];
			lookup.sort();
			var key = lookup.join("_");
			if (! cfg[key] ) 
				cfg[key] = 0;
			cfg[key]  += chg * vals[3];
		});
		ppl = Object.keys(ppl);
		for ( var i in cfg ) 
		{
			outStr.push(i + "::" + cfg[i]);	
		}

		outStr.push(JSON.stringify(ppl));
		 for ( var i = 0; i < ppl.length; i++ )
		{
			   calcRoute(ppl, i, ppl[i], 0)

		}
		var min = undefined;
		var minTrip = "";
		for ( var i in trips )
		{
			if ( min === undefined )
			{
				min=trips[i];
				minTrip = i;
			} else if ( min > trips[i] ) {
				minTrip = i;
				min= trips[i];

			}
		}

		var max = undefined;
		var maxTrip = "";
		for ( var i in trips )
		{
			if ( max === undefined )
			{
				max=trips[i];
				maxTrip = i;
			} else if ( max < trips[i] ) {
				maxTrip = i;
				max= trips[i];

			}
		}


		outStr.push("Min Trip:" +   minTrip + "::" + min );
		outStr.push("max trip:" + maxTrip + "::" + max);

		res.value = outStr.join("\n");
	},
	Day13Part2: function()
	{
		var calcRoute = function(arrIn, elm, currentPath, currentDist)
		{
			var newArr = arrIn.slice();

			var curCit = newArr[elm];

			newArr.splice(elm,1);
			if (  newArr.length === 0  ) 
			{

				var elm = currentPath.split("_");
				var tmpElm = [elm[0], elm[elm.length-1]];
				tmpElm.sort();
				trips[currentPath] =currentDist + cfg[tmpElm.join("_")];
				//trips[currentPath] = currentDist;
				return;
			}
			for ( var i = 0; i < newArr.length; i++)
			{
				var elm = [curCit, newArr[i]];
				elm.sort();
				if ( cfg[elm.join("_")] !== undefined )
				{
					calcRoute(newArr, i, currentPath + "_" + newArr[i], currentDist +  cfg[elm.join("_")]);							
				} else {
					return;	
				}
			}


		}

		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		/* [0] is Person [2] gain lose [3] value [10] person followed by ending . */
		var ppl = {};
		var cfg = {};
		var trips = {};
		var dist = {};
		vals.forEach(function(itm) {

			var vals = itm.split(" ");
			ppl[vals[0]] = true;
			var chg = 1;
			if ( vals[2] == "lose" )
				chg = -1;
			var lookup = [ vals[0],  vals[10].slice(0,-1)];
			lookup.sort();
			var key = lookup.join("_");
			if (! cfg[key] ) 
				cfg[key] = 0;
			cfg[key]  += chg * vals[3];
		});

		ppl = Object.keys(ppl);
		ppl.forEach(function(itm) {
			var elm = ["Mike", itm];
			elm.sort();
			cfg[elm.join("_")] = 0;
		});
		ppl.push("Mike");
		for ( var i in cfg ) 
		{
			outStr.push(i + "::" + cfg[i]);	
		}
		 for ( var i = 0; i < ppl.length; i++ )
		{
			   calcRoute(ppl, i, ppl[i], 0)


		}
		var min = undefined;
		var minTrip = "";
		for ( var i in trips )
		{
			if ( min === undefined )
			{
				min=trips[i];
				minTrip = i;
			} else if ( min > trips[i] ) {
				minTrip = i;
				min= trips[i];

			}
		}

		var max = undefined;
		var maxTrip = "";
		for ( var i in trips )
		{
			if ( max === undefined )
			{
				max=trips[i];
				maxTrip = i;
			} else if ( max < trips[i] ) {
				maxTrip = i;
				max= trips[i];

			}
		}


		outStr.push("Min Trip:" +   minTrip + "::" + min );
		outStr.push("max trip:" + maxTrip + "::" + max);

		res.value = outStr.join("\n");
	},
	Day14Part1: function()
	{
				var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		var reindeer = {};

		var time = parseInt(vals[vals.length-1]);
		for ( var i = 0; i < vals.length-1; i++ )
		{
			var elm = vals[i].split(" ");
			var tmpData = {speed: elm[3]*1, endurance:elm[6]*1, rest: elm[13]*1};
			tmpData["distPer"] =  tmpData.speed * tmpData.endurance;				
			tmpData["time"] = tmpData.endurance + tmpData.rest;
			var tmpTime = time % tmpData["time"];
			var tmpDist = 0;
			if ( tmpTime > tmpData.endurance ) 
				tmpDist += tmpData.speed * tmpData.endurance;
			else 
				tmpDist += tmpData.speed * tmpTime;

			tmpDist += Math.floor(time / tmpData["time"] ) * tmpData.distPer;
			tmpData["totDist"] = tmpDist;

			reindeer[elm[0]] = tmpData;

		}

		for ( var i in reindeer ) 
		{
			outStr.push(i + "::" + reindeer[i].totDist);	
		}
		res.value = outStr.join("\n");
	},
	Day14Part2: function()
	{
			var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
				var reindeer = {};
		var distAtTime=function(reindeer, time )
		{
			var tmpTime = time % reindeer["time"];
			var tmpDist = 0;
			if ( tmpTime > reindeer.endurance ) 
				tmpDist += reindeer.speed * reindeer.endurance;
			else 
				tmpDist += reindeer.speed * tmpTime;

			tmpDist += Math.floor(time / reindeer["time"] ) * reindeer.distPer;
			return tmpDist;	
		}
		var time = parseInt(vals[vals.length-1]);
		for ( var i = 0; i < vals.length-1; i++ )
		{
			var elm = vals[i].split(" ");
			var tmpData = {speed: elm[3]*1, endurance:elm[6]*1, rest: elm[13]*1};
			tmpData["distPer"] =  tmpData.speed * tmpData.endurance;				
			tmpData["time"] = tmpData.endurance + tmpData.rest;
			var tmpTime = time % tmpData["time"];
			var tmpDist = 0;
			if ( tmpTime > tmpData.endurance ) 
				tmpDist += tmpData.speed * tmpData.endurance;
			else 
				tmpDist += tmpData.speed * tmpTime;

			tmpDist += Math.floor(time / tmpData["time"] ) * tmpData.distPer;
			tmpData.points = 0;
			reindeer[elm[0]] = tmpData;

			//tmpData["totDist"] = distAtTime(reindeer[elm[0]],time);				


		}
		var rArr  = Object.keys(reindeer);
		for ( var i = 1; i <= time; i++ )
		{
			var ranking = [];
			rArr.forEach(function(deer) {
			ranking.push({deer:deer, dist:distAtTime(reindeer[deer], i ) } ); } );
			ranking.sort(function(a,b) { if ( a.dist < b.dist ) return -1;
											if ( a.dist > b.dist ) return 1;
										return 0;} );
			var maxDist = ranking[ranking.length-1].dist;

			for ( var j = ranking.length - 1; j >= 0; j-- )
			{					
				if ( maxDist === ranking[j].dist )
					reindeer[ranking[j].deer].points++;
				else
					break;
			}

		}

		for ( var i in reindeer ) 
		{
			outStr.push(i + "::" + reindeer[i].points);	
		}
		res.value = outStr.join("\n");
	},
	Day15Part1: function()
	{
			var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		var scoreIngredients= function(ingredients, ratio) {
			var tot = 1;
			var elms = ["cap", "dur", "fla", "txt"];
			elms.some( function(el) {
				var subTot = 0;
				for (var ing in  ingredients )
				{
					if ( ratio[ing] )
						subTot += ingredients[ing][el] * ratio[ing];
				}

				if ( subTot <= 0)
				{
					tot=0;
					subTot = 0;
					return true;
				}
				tot *= subTot;

				return false;

			});
			totCal = 0;
			for (var ing in  ingredients )
				{
					if ( ratio[ing] )
						totCal += ingredients[ing]["cal"] * ratio[ing];
				}

			return tot;	
		}
		var getCombos = function(level, valLeft, maxLevels)
			{
				for ( var l = 0; l <= valLeft; l++ )
				{
					if (level == 1 )
						ratio = {

			};

					ratio[ings[level-1]] = l;
					if ( level == maxLevels -1)
					{
						ratio[ings[level]] = valLeft - l;

						var tmpScore = scoreIngredients(ingredients, ratio);
						if ( maxScore <= tmpScore )
						{
							maxRatio = JSON.stringify(ratio);
							maxScore = tmpScore;	
						}
						numLoops++;
					} else {
						getCombos(level+1,valLeft - l, maxLevels);

					}


				}
			}
		var ingredients = {};
		vals.forEach(function(elm) { 
			/* ing . n .  */
			var elms  = elm.split( " " ) ;
			var ing = elms[0].slice(0,-1);
			var tmpElm = {cap: elms[2].slice(0,-1) * 1, dur: elms[4].slice(0,-1) * 1, fla : elms[6].slice(0,-1) * 1, txt: elms[8].slice(0,-1) * 1, cal: elms[10]*1};
			ingredients[ing] = tmpElm;

		} );
		var ings = Object.keys(ingredients);
		var ratio = {"Sugar" : 44, "Sprinkles" : 56 } ;

		var totTouse = 100;
		var maxScore = 0;
		var maxRatio = "";
		var numLoops = 0;

		getCombos(1, totTouse, ings.length );

		outStr.push(maxRatio);
		outStr.push(maxScore);

		res.value = outStr.join("\n");
	},
	Day15Part2: function()
	{
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		var scoreIngredients= function(ingredients, ratio) {
			var tot = 1;
			var elms = ["cap", "dur", "fla", "txt"];
			elms.some( function(el) {
				var subTot = 0;
				for (var ing in  ingredients )
				{
					if ( ratio[ing] )
						subTot += ingredients[ing][el] * ratio[ing];
				}

				if ( subTot <= 0)
				{
					tot=0;
					subTot = 0;
					return true;
				}
				tot *= subTot;

				return false;

			});
			totCal = 0;
			for (var ing in  ingredients )
			{
				if ( ratio[ing] )
					totCal += ingredients[ing]["cal"] * ratio[ing];
			}
			if ( totCal !== 500)
			{
				tot = 0;
			}
			return tot;	
		}
		var getCombos = function(level, valLeft, maxLevels)
		{
			for ( var l = 0; l <= valLeft; l++ )
			{
				if (level == 1 )
					ratio = {};
				ratio[ings[level-1]] = l;
				if ( level == maxLevels -1)
				{
					ratio[ings[level]] = valLeft - l;

					var tmpScore = scoreIngredients(ingredients, ratio);
					if ( maxScore <= tmpScore )
					{
						maxRatio = JSON.stringify(ratio);
						maxScore = tmpScore;	
					}
					numLoops++;
				} else {
					getCombos(level+1,valLeft - l, maxLevels);

				}


			}
		}
		var ingredients = {};
		vals.forEach(function(elm) { 
			
			var elms  = elm.split( " " ) ;
			var ing = elms[0].slice(0,-1);
			var tmpElm = {
				cap: elms[2].slice(0,-1) * 1, 
				dur: elms[4].slice(0,-1) * 1, 
				fla : elms[6].slice(0,-1) * 1, 
				txt: elms[8].slice(0,-1) * 1, 
				cal: elms[10]*1
			};
			ingredients[ing] = tmpElm;
		} );
		var ings = Object.keys(ingredients);
		var ratio = {"Sugar" : 44, "Sprinkles" : 56 } ;

		var totTouse = 100;
		var maxScore = 0;
		var maxRatio = "";
		var numLoops = 0;

		getCombos(1, totTouse, ings.length );

		outStr.push(maxRatio);
		outStr.push(maxScore);

		res.value = outStr.join("\n");
	},
	
	Day16Part1 : function()
	{
	var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
	var sues = [];
	var ticker={};	
	var loadingSues = false;
	vals.forEach(function(val) { 
		if ( val == "*" )
			loadingSues = true;
		else if ( ! loadingSues )
		{
			var elm = val.split(" ");
			ticker[elm[0]] = parseInt(elm[1]);
		} else {
			var elm = val.split(" ");
			var newSue = {
			};
			for ( var i = 2; i <= elm.length; i = i + 2 )
			{
				newSue[elm[i]] = parseInt(elm[i+1]);	
			}
			sues.push(newSue);
		}
	} );
	var sueFound = 0;
	sues.some(function(val, ndx) {
		for ( var i in ticker )
		{
			
			if ( val[i] !== undefined )
			{
			
				if ( val[i] !== ticker[i] )
				{
					return false;
				}
			}
		}
		
		sueFound = ndx+1;
		return true;
	});
		outStr.push(sueFound);
	outStr.push(JSON.stringify(sues));
	outStr.push(JSON.stringify(ticker));
	res.value = outStr.join("\n");
	
},
	Day16Part2 : function()
{
	var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		var comparator = {"cats:" : "gt", "trees:" : "gt", "pomeranians:" : "lt", "goldfish:" : "lt" }
	
	var sues = [];
	var ticker={};	
	var loadingSues = false;
	vals.forEach(function(val) { 
		if ( val == "*" )
			loadingSues = true;
		else if ( ! loadingSues )
		{
			var elm = val.split(" ");
			ticker[elm[0]] = parseInt(elm[1]);
		} else {
			var elm = val.split(" ");
			var newSue = {
			};
			for ( var i = 2; i <= elm.length; i = i + 2 )
			{
				newSue[elm[i]] = parseInt(elm[i+1]);	
			}
			sues.push(newSue);
		}
	} );
	var sueFound = 0;
	sues.some(function(val, ndx) {
		for ( var i in ticker )
		{
			
			if ( val[i] !== undefined )
			{
				if ( comparator[i] ) 
				{
					if ( comparator[i] == "gt" ) 
					{
						if ( val[i] <= ticker[i] )
							return false;
					}
					else if ( comparator[i] == "lt" )
					{
						if ( val[i] >= ticker[i] )
							return false;
					}
				} else if ( val[i] !== ticker[i] )
				{
					return false;
				}
			}
		}
		
		sueFound = ndx+1;
		return true;
	});
		outStr.push(sueFound);
	outStr.push(JSON.stringify(sues));
	outStr.push(JSON.stringify(ticker));
	res.value = outStr.join("\n");
	
	
	
},
Day17Part1: function()
{
	var numSort = function(a,b) {
		if ( a < b) 
			return -1; 
		else if ( a > b ) 
			return 1; 
		else 
			return 0;
	}
	
	
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		var totalSize = Number(vals[0]);
		var ratio = {};
		vals.splice(0,2);
		var sizes = [];
		var fitCombos = [];
		vals.forEach(function(val) {sizes.push(Number(val))})
		sizes.sort(numSort);
		var matches = [];
		var combos = function(currentCombo, sizeIndex, currentCost)
		{
							for ( var i = sizeIndex+1; i < sizes.length; i++)
				{
					if (  totalSize == currentCost + sizes[i])
					{
						matches.push(currentCombo + "_" + sizes[i]);
					}	
					else if ( totalSize > currentCost + sizes[i])
					{
						combos(currentCombo + "_"+ sizes[i], i, currentCost + sizes[i] )
					}
				}
		}
		combos("", -1, 0);
		outStr.push("Matches:" + matches.length)
		outStr.push(JSON.stringify(matches))
		/* 
			We need to try all combos
			 combos(currentCombo, itemsLeft)
			 {
				 if ( sum < totalSize )
				 {
					 try adding remaining items
				 }else if  ( sum === totalSize)
				 	add item as complete
				} else {
					this sum is too much exit
				}
			 }
		*/
		
		outStr.push(JSON.stringify(sizes));
		res.value = outStr.join("\n");
},
Day17Part2: function() 
{
	
	
	var numSort = function(a,b) {
		if ( a < b) 
			return -1; 
		else if ( a > b ) 
			return 1; 
		else 
			return 0;
	}
	
	
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		var totalSize = Number(vals[0]);
		var ratio = {};
		vals.splice(0,2);
		var sizes = [];
		var fitCombos = [];
		vals.forEach(function(val) {sizes.push(Number(val))})
		sizes.sort(numSort);
		var matches = [];
		var combos = function(currentCombo, sizeIndex, currentCost)
		{
							for ( var i = sizeIndex+1; i < sizes.length; i++)
				{
					if (  totalSize == currentCost + sizes[i])
					{
						matches.push(currentCombo + "_" + sizes[i]);
					}	
					else if ( totalSize > currentCost + sizes[i])
					{
						combos(currentCombo + "_"+ sizes[i], i, currentCost + sizes[i] )
					}
				}
		}
		combos("", -1, 0);
		var min = undefined;
		var numMin = 0;
		matches.forEach(function(elm) { 
			var tmpLen = elm.split("_").length;
			if ( min === undefined ) 
		{
		min = tmpLen;
		numMin = 1;
		} else if ( min === tmpLen ) {
			numMin++;
		} else if ( min > tmpLen ) {
			min=tmpLen;
			numMin = 1;
		}								
		})
		outStr.push("Matches:" + numMin + " of " + min + " length");
		//outStr.push(JSON.stringify(matches))
		/* 
			We need to try all combos
			 combos(currentCombo, itemsLeft)
			 {
				 if ( sum < totalSize )
				 {
					 try adding remaining items
				 }else if  ( sum === totalSize)
				 	add item as complete
				} else {
					this sum is too much exit
				}
			 }
		*/
		
		outStr.push(JSON.stringify(sizes));
		res.value = outStr.join("\n");
},
Day18Part1: function()
{
	var outputCfg= function(cfgIn) {
		var arrOut = [];
		cfgIn.forEach(function(elm) {arrOut.push(elm.join(""))})
		var str= arrOut.join("\n");
		str = str.replace(/1/gi, "#");
		str = str.replace(/0/gi, ".");
		return str;
	}
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		var totalSize = Number(vals[0]);
		vals.splice(0,1);
		var cfg = [];
		vals.forEach(function(itm) {
			cfg[cfg.length]= [];
			itm.split("").forEach(function(elm) { if ( elm  == "." ) { cfg[cfg.length-1].push(0) } else { cfg[cfg.length-1].push(1)}});
			
		});
		var calcNext = function(cfgIn, numLeft ) 
		{
			var nxtB = [];
			for ( var i = 0; i < cfgIn.length; i++ )
			{
				var tmp = [];
				for ( var j =0; j < cfgIn[i].length; j++)
				{
					var sum = 0;
					if ( i > 0 )
					{
						sum += cfgIn[i-1][j];
					}
					if ( i < cfgIn.length - 1)
					{
						sum += cfgIn[i+1][j];
					}
					if ( j > 0 )
					{
						sum += cfgIn[i][j-1];
					}
					if ( j < cfgIn[i].length - 1)
					{
						sum += cfgIn[i][j+1];
					}
					if ( i > 0 && j > 0 )
					{
						sum += cfgIn[i-1][j-1];
					}
					if ( i > 0 && j < cfgIn[i].length - 1)
					{
						sum += cfgIn[i-1][j+1];
					}
					if ( i < cfgIn.length - 1 && j > 0)
					{
						sum += cfgIn[i+1][j-1];
					}
					if ( i < cfgIn.length - 1 && j < cfgIn[i].length - 1)
					{
						sum+= cfgIn[i+1][j+1];
					}
					if ( sum === 3 )
					{
						tmp[j] = 1;
					} else if ( cfgIn[i][j] === 1)
					{
						if ( sum === 2)
						{
							tmp[j] = 1;
						} else {
							tmp[j] = 0;
						}
					} else {
						tmp[j] = 0;
						
					}
				}
				nxtB.push(tmp);
					
			}
			nxtB[0][0] = 1;
			nxtB[0][nxtB[0].length-1] = 1;
			nxtB[nxtB.length-1][0] = 1;
			nxtB[nxtB.length-1][nxtB[0].length-1] = 1;
			
			
			if ( numLeft > 1 )
				calcNext(nxtB, numLeft - 1)
			else
			{
				var totVal = 0;
				for ( var i = 0; i < nxtB.length; i++ )
					for ( var j = 0; j <nxtB[i].length; j++)
						totVal += nxtB[i][j];
				//outStr.push(outputCfg(nxtB));
				outStr.push("Total Val: " + totVal)
			}
		}
		//outStr.push(outputCfg(cfg));
		outStr.push("****");
		calcNext(cfg, totalSize);
		res.value = outStr.join("\n");
},
Day18Part2: function() {
	var outputCfg= function(cfgIn) {
		var arrOut = [];
		cfgIn.forEach(function(elm) {arrOut.push(elm.join(""))})
		var str= arrOut.join("\n");
		str = str.replace(/1/gi, "#");
		str = str.replace(/0/gi, ".");
		return str;
	}
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		var totalSize = Number(vals[0]);
		vals.splice(0,1);
		var cfg = [];
		vals.forEach(function(itm) {
			cfg[cfg.length]= [];
			itm.split("").forEach(function(elm) { if ( elm  == "." ) { cfg[cfg.length-1].push(0) } else { cfg[cfg.length-1].push(1)}});
			
		});
		var calcNext = function(cfgIn, numLeft ) 
		{
			
			cfgIn[0][0] = 1;
			cfgIn[0][cfgIn[0].length-1] = 1;
			cfgIn[cfgIn.length-1][0] = 1;
			cfgIn[cfgIn.length-1][cfgIn[0].length-1] = 1;
			
			
			var nxtB = [];
			for ( var i = 0; i < cfgIn.length; i++ )
			{
				var tmp = [];
				for ( var j =0; j < cfgIn[i].length; j++)
				{
					var sum = 0;
					if ( i > 0 )
					{
						sum += cfgIn[i-1][j];
					}
					if ( i < cfgIn.length - 1)
					{
						sum += cfgIn[i+1][j];
					}
					if ( j > 0 )
					{
						sum += cfgIn[i][j-1];
					}
					if ( j < cfgIn[i].length - 1)
					{
						sum += cfgIn[i][j+1];
					}
					if ( i > 0 && j > 0 )
					{
						sum += cfgIn[i-1][j-1];
					}
					if ( i > 0 && j < cfgIn[i].length - 1)
					{
						sum += cfgIn[i-1][j+1];
					}
					if ( i < cfgIn.length - 1 && j > 0)
					{
						sum += cfgIn[i+1][j-1];
					}
					if ( i < cfgIn.length - 1 && j < cfgIn[i].length - 1)
					{
						sum+= cfgIn[i+1][j+1];
					}
					if ( sum === 3 )
					{
						tmp[j] = 1;
					} else if ( cfgIn[i][j] === 1)
					{
						if ( sum === 2)
						{
							tmp[j] = 1;
						} else {
							tmp[j] = 0;
						}
					} else {
						tmp[j] = 0;
						
					}
				}
				nxtB.push(tmp);
					
			}
			nxtB[0][0] = 1;
			nxtB[0][nxtB[0].length-1] = 1;
			nxtB[nxtB.length-1][0] = 1;
			nxtB[nxtB.length-1][nxtB[0].length-1] = 1;
			
				outStr.push("*****");
			
//				outStr.push(outputCfg(nxtB));
			
			if ( numLeft > 1 )
				calcNext(nxtB, numLeft - 1)
			else
			{
				var totVal = 0;
				for ( var i = 0; i < nxtB.length; i++ )
					for ( var j = 0; j <nxtB[i].length; j++)
						totVal += nxtB[i][j];
					outStr.push(outputCfg(nxtB));
					outStr.push("Total Val: " + totVal)
			}
		}
		//outStr.push(outputCfg(cfg));
		outStr.push("****");
		calcNext(cfg, totalSize);
		res.value = outStr.join("\n");
},
/*
	Day19Part1: function()
	{
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		
		var startElm = vals.splice(-2, 2).pop()
		var strElms ={};
		var elmNums = []
		
		vals.forEach(function(val) {
			var tmpArr = val.split(" => ");
			if (! strElms[tmpArr[0]])
			{
				strElms[tmpArr[0]] = [];
				elmNums.push(tmpArr[0]);
			}
			strElms[tmpArr[0]].push(tmpArr[1]);
		} );
		
		var transElm = startElm;
		elmNums.forEach(function(itm, indx) {
			var indxStr= ("_000000000"+ indx);
			indxStr = indxStr.substr(indxStr.length-4);
			transElm = transElm.replace(new RegExp(itm, "g"), "{" + indxStr + "}");
			
		})
		var regexp = /([^{]*)(\{\d{4}\})([^{]*)/g 
		var itms = transElm.match(regexp);
		
		var currentOutStr = {"": ""}
		var itmInside, i, j, itm, newOutStr;		
		while (itms.length > 0 )
		{
			var keys = Object.keys(currentOutStr);
			
			 newOutStr = {};
			
			if ( itms.length === 0 )
			{
				outStr.push("Len:" +  keys.length);
			} else {
				var curItm = itms.shift();
				var newOutStr = {};
				var bef = "";
				var aft = "";
				var srchRegExp = /(\{\d{4}\})/
				var thisKey = curItm.search(srchRegExp);
				if ( thisKey > 0)
				{
					bef = curItm.substr(0,thisKey-1);
											
				}
				if ( thisKey > -1)
				{
					aft = curItm.substr(thisKey+6);			
				}
				
				thisKey = Number(curItm.substr(thisKey+1,4))
				
				for (  i = 0; i < keys.length; i++ )
				{
					 itm = keys[i];
					
					if ( elmNums[thisKey])
					{
						for ( j = 0; j< strElms[elmNums[thisKey]].length; j++)
						{
							itmInside = strElms[elmNums[thisKey]][j];
						
							newOutStr[itm + bef + itmInside + aft] = true;
						}
					}
				}	
				currentOutStr = {};
				for(var i in newOutStr) {
					if(newOutStr.hasOwnProperty(i)) {
						currentOutStr[i] = newOutStr[i];
					}
				}		
			}
			/*
				get next item in the itms array
			 
		}
			outStr.push("Len:" +  Object.keys(newOutStr).length);
		
		
			We have an array of matching items
			now we go through the str and match
			first time 
				0009 has two items string now has two copies
				
		
		
		outStr.push(startElm);
		outStr.push(transElm);
		//outStr.push(strElms);
		
		res.value = this.frmOutStr(outStr);
	}, */
	Day19Part1: function() 
	{
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		
var molecule = vals.splice(-2, 2).pop();
var replacementsStr = vals.join("\n");

		
var replacements = [];

replacementsStr.split('\n').forEach((line) => {
    var match = /(\w+) => (\w+)/.exec(line);
    replacements.push({
        from: match[1],
        to:   match[2]
    });
});

var  generateSolutions = function(input, backwards) {
    var solutions = [];
    var uniqueSolutions = [];

    for (var j = replacements.length - 1; j >= 0; j--) {
        var replacement = replacements[j];
        var from = backwards ? replacement.to   : replacement.from;
        var to   = backwards ? replacement.from : replacement.to;
        var prefix = '';
        var piece = input;
        var i;

        while ((i = piece.indexOf(from)) !== -1) {
            var endIndex = i + from.length;
            var solution = prefix + piece.slice(0, i) + to + piece.slice(endIndex);
            solutions.push(solutions);

            if (uniqueSolutions.indexOf(solution) === -1)
                uniqueSolutions.push(solution);

            prefix += piece.slice(0, endIndex);
            piece = piece.slice(endIndex);
        }
    }

    return uniqueSolutions;
}


outStr.push('Part One:', generateSolutions(molecule).length);
		
		
		res.value = outStr.join("\n");
	},
	Day19Part2: function() 
	{
		
		
var  generateSolutions = function(input, backwards) {
    var solutions = [];
    var uniqueSolutions = [];

    for (var j = replacements.length - 1; j >= 0; j--) {
        var replacement = replacements[j];
        var from = backwards ? replacement.to   : replacement.from;
        var to   = backwards ? replacement.from : replacement.to;
        var prefix = '';
        var piece = input;
        var i;

        while ((i = piece.indexOf(from)) !== -1) {
            var endIndex = i + from.length;
            var solution = prefix + piece.slice(0, i) + to + piece.slice(endIndex);
            solutions.push(solutions);

            if (uniqueSolutions.indexOf(solution) === -1)
                uniqueSolutions.push(solution);

            prefix += piece.slice(0, endIndex);
            piece = piece.slice(endIndex);
        }
    }

    return uniqueSolutions;
}

		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		
		var molecule = vals.splice(-2, 2).pop();
var replacementsStr = vals.join("\n");

		
var replacements = [];
replacementsStr.split('\n').forEach((line) => {
    var match = /(\w+) => (\w+)/.exec(line);
    replacements.push({
        from: match[1],
        to:   match[2]
    });
});

var sliceCount = 7; /* Increase this is no solution is found */
var molecules = [molecule];
outStr.push(JSON.stringify(molecules))
var resultMolecules = [];
var allSolutions = [];
var steps = 0;

while (molecules.length && molecules.indexOf('e') === -1) {

    resultMolecules = [];
	
outStr.push(JSON.stringify(molecules));
    for (var i = molecules.length - 1; i >= 0 && resultMolecules.length < sliceCount; i--) {
        var solns = generateSolutions(molecules[i], true);
		  for (var j = solns.length - 1; j >= 0; j--) {
            var soln = solns[j];
				
            if ((soln.length === 1 || soln.indexOf('e') === -1) && allSolutions.indexOf(soln) === -1) {
                resultMolecules.push(soln);
                allSolutions.push(soln);
            }
        }
    }
	
    molecules = resultMolecules;
    steps++;


    // Take only a few of the shortest molecules
    var shortest = Infinity;
    molecules.forEach((molecule) => {
        shortest = Math.min(shortest, molecule.length);
    });

    molecules = molecules.filter(s => s.length === shortest).slice(0, sliceCount);
	
}

		outStr.push('Part Two:', molecules.length ? steps : 'No solution found');
		res.value = outStr.join("\n");
	},
	frmOutStr: function(inStr)
	{
		var newArr = [];
		for(var i = 0; i < inStr.length; i++)
		{
			if ( typeof inStr[i] === "object" )
			{
				newArr[i] = JSON.stringify(inStr[i]);
			} else {
				newArr[i] = inStr[i];
			}
		}
		return newArr.join("\n");
		
	}
} 
/*
	DayXPart1: function()
	{
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		
		
		res.value = outStr.join("\n");
	},
	DayXPart2: function() 
	{
		var vals = document.getElementById("inp").value.split("\n");
		var res = document.getElementById("res");
		var outStr = [];
		
		
		
		res.value = outStr.join("\n");
	}
*/

Advent.LoadReadMe();